# Library Migrator使用指南

#### 介绍
对于准备将设计流程完全迁移至DigiPCBA平台上的工程师来说，可能遇到的第一个挑战就是如何将手上的元件库迁移至云端。这篇教程将会展示如何使用Altium Designer软件提供的Library Migrator工具，完成一个新建Workspace的初始设置，随后将本地元件库迁移至其上。

Library Migrator支持所有的元件库类型。需要迁移的源数据可以是集成库，DB Library，也可以是独立的原理图符号库。无论来源库的类型如何，操作都大同小异。下面的介绍会以较有代表性的DB Library为示例。示例用的元件库可以在Gitee上找到 [library-migrator-user-guide](https://gitee.com/contentfrank/library-migrator-user-guide)


#### 创建文件夹结构
Workspace创建时默认设置的文件夹结构比较简单，对于大型的元件库来说很多时候都不能满足需求。我们可以从界面右下角的Panels中选择Explorer打开游览器来游览和修改Workspace中内容。

![start_explorer](https://images.gitee.com/uploads/images/2021/1027/105034_949a745f_9514082.png "start_explorer.png")

以Capacitor文件夹为例，可以看见默认创建的Capacitor文件夹只有一个，而电容往往数量庞大。一个大型的元件库可能有几千到几万条关于电容的记录。如果所有的电容都迁移至这个文件夹内，那么后期无论是游览或者维护其中内容的效率都会被大大拖慢。

![vault_folder_list](https://images.gitee.com/uploads/images/2021/1027/105204_b7c440d6_9514082.png "vault_folder_list.png")

类似的晶体管相关的文件夹也只有Transistor一个。而我们准备迁移的元件库中包含了四种类型的MOSFET，并且希望每种MOSFET都被迁移到对应的文件夹。所以我们需要自己丰富预设的文件夹结构。右击Transistor文件夹，选择Add Subfolder。由于Transistor的文件夹类型是Component，所以默认创建的子文件夹类型也与其保持一致。如果需要不同类型的文件夹类型，可以在Folder Type中进行修改。

![change_folder_type](https://images.gitee.com/uploads/images/2021/1027/105308_45e7bdbb_9514082.png "change_folder_type.png")

我们在Transistor下面新建MOSFET，P-Channel, N-Channel, Multi-Channel和SCR四个子文件夹，结构如下图所示

![mos_folder_struct](https://images.gitee.com/uploads/images/2021/1027/105351_d5a0e8d5_9514082.png "mos_folder_struct.png")

#### 设置器件类型

调用云端的器件可以通过在Explorer中右键器件然后选择Place放置。不过更方便的方式是通过界面右侧的Components面板进行搜索和调用。点击面板顶部的All，展开器件类型结构可以看见默认创建的器件类型定义。

![default_create_cmp_types](https://images.gitee.com/uploads/images/2021/1027/105446_0f3e7db3_9514082.png "default_create_cmp_types.png")

值得注意的是这里显示的树状结构和Explorer中看见的文件夹结构并无关联。我们刚才创建的MOSFET相关的目录结构并没有出现。实际上这里显示的结构完全取决于器件类型的定义。
并且与默认创建的文件夹结构类似，默认创建的器件类型比较简单，难以满足大型元件库的需求。为了能在Component面板中显示与Explorer中类似的目录结构我们需要自己创建一些新的分类。点击右上角的齿轮图标，打开Preference对话框，在Data Management下面找到Component Types。在这里我们可以观察到器件类型之间的关联决定了Components面板上显示的层次结构。
![def_cmp_types](https://images.gitee.com/uploads/images/2021/1027/105545_6e085288_9514082.png "def_cmp_types.png")

我们右击Transistor为其添加一系列子类型。
![add_sub_types_cap](https://images.gitee.com/uploads/images/2021/1027/105634_153853f9_9514082.png "add_sub_types_cap.png")


#### 创建模板
在定义器件类型时，我们看见在Name边上还有两列数据，Template和Default Folder。Default Folder定义了该类型器件的默认上传路径。这个参数无法直接设置，但是我们可以通过将器件类型和模板绑定来间接的定义发布路径。

如果要为某个器件类型绑定一个创建好的模板，可以通过Component Types左下方的Templates进行指定。

而要创建一个新的模板，则需要回到Explorer中进行。依次点开Managed Content>>Templates>>Component Templates。点击右上角Add Template来创建一个新模板![add_template_button](https://images.gitee.com/uploads/images/2021/1027/105807_f535c622_9514082.png "add_template_button.png")
模板里有许多关于云端器件的预定义设置，我们先来看看这次要用到的两项。在Default Folder一项中可以设置默认的器件上传路径，这也是此前我们在Component Types中看见的Defautl Folder的内容来源。而Parameters中的ComponentType可以绑定已经创建的器件类型。
对于刚才创建的器件类型，我们分别为每个类型都创建一个模板
![edit_template](https://images.gitee.com/uploads/images/2021/1027/105908_e98ae0cd_9514082.png "edit_template.png")

完成编辑之后，点击Save![save_button](https://images.gitee.com/uploads/images/2021/1027/105945_8073a43c_9514082.png "save_button.png") 将模板文件保存至云端。然后我们回到Component Types页面，会发现器件类型和模板已经一一对应。
![component_types_match_templates](https://images.gitee.com/uploads/images/2021/1027/110102_fefab142_9514082.png "component_types_match_templates.png")

至此我们已经完成两项重要的准备工作，可以进入迁移流程。

#### 简单模式
打开准备迁移的DB Library文件(mosfet_db.DbLib)。 从菜单栏File>>Migrate Library启动migrator, 通过此种方式启动的Migrator默认进入Simple Mode。在此模式下，我们只需要点击右下角的Migrate按钮就可以轻松的将器件发布至云端，无需担心任何参数配置问题。
![simple_mode](https://images.gitee.com/uploads/images/2021/1027/110209_a76c83ae_9514082.png "simple_mode.png")

但是在很多情况下，默认发布配置并不能满足我们的需求。用户需要更精准的器件分类，更复杂的Lifecycle设置，结构层次更多的文件路径。此时我们可以通过点击页面上的settings或者左下角的Advanced按钮来打开Properties对话框来进行详细的配置。


#### 高级模式
在进入Advance模式之后，可以看见Library Migrator尝试对元件库中的器件进行了自动分类，示例中的MOSFET没有正确的被Migrator识别，因此类别显示为Uncategorized。通过点击准备迁移的器件，可以在下方的Details区域中查看到将和这个器件一起被迁移的关联数据，包括Models，Datasheets和供应链信息。
![输入图片说明](https://images.gitee.com/uploads/images/2021/1027/110609_388f81fe_9514082.png "advanced_mode.png")


#### 云端相关参数配置
接下来打开Properties对话框（快捷键 F11），点击Advanced标签。在这里我们可以定义迁移器件的目的地，命名格式，Lifecycle等一系列和Workspace相关的参数。其中Naming Scheme，Revsion Naming Schema和Lifecycle这几个参数一般保持默认设置即可。

Library Migrator在执行迁移之前会检查器件中是否有重复，其检查的对象可以在Unique Field中设置。这里我们保持和DB Library相同的设置，使用Name作为索引，或者也可以指定参数Partnubmer作为索引。

正如我们看见的，器件的迁移目的地可以通过Component相关的参数配置。但是我们暂时不做修改，只将Symbol和Footprint的目标迁移地址修改为我们需要的文件夹。原因在下面的教程中会有解释。
![advanced_change_sym_ftp_dest](https://images.gitee.com/uploads/images/2021/1027/110724_0a210509_9514082.png "advanced_change_sym_ftp_dest.png")


#### 器件类型与迁移路径
这两项在迁移元件库时都是重要的参数，两者可以分别在Properties的General和Advanced标签下设置。值得注意的是在Properties中配置的参数的作用域是正在迁移的整个库文件，不能指定对元件库中的一部分器件生效。这导致了一个问题，也就是我们可以将元件库中的所有器件一起迁移到某个路径下，但是在一次迁移操作中，无法将器件按照其类型分别迁移到相应的路径下。而这恰恰是在迁移元件库时是非常常见的需求。

这时候我们在开始阶段定义的器件模板就能发挥作用了。在定义模板时我们对器件路径做出了预定义，接着将器件分类和模板进行绑定。这样我们就能通过为某个器件指定一种类型来间接的确定他应该被迁移到哪个路径下。
![cmp_template_default_folder](https://images.gitee.com/uploads/images/2021/1027/110828_848744ba_9514082.png "cmp_template_default_folder.png")

关闭Properties对话框回到Library Migrator，我们按住Shift一次选中多行器件。右击选中内容执行Change Component Type，为器件选择正确的分类。然后我们可以看到Folder一列已经更改为我们想要的迁移路径了。我们依次为所有的器件都指定好分类。
![right_clieck_change_type](https://images.gitee.com/uploads/images/2021/1027/110940_53c534b5_9514082.png "right_clieck_change_type.png")
![column_folder_change](https://images.gitee.com/uploads/images/2021/1027/111003_ec297c0d_9514082.png "column_folder_change.png")


#### 另一种对器件分类的方法
我们在前面看到，每次向Library Migrator中添加元件库的时候，Migrator都会尝试对器件分类。这个分类是通过对器件的Designator，描述以及参数中的一些关键字进行分析来实现的。得到的分类仅限于预设的目录而且往往不太准确。所以通常我们需要对分类结果作出调整。当需要迁移的元件库比较少，而且库中的器件分类比较合理的时候这项工作还比较轻松。但是当需要迁移的元件库数量庞大，每个元件库又包括多种类型的器件时，为每个器件指定分类就变成一项痛苦的工作了。

想要在迁移时快速完成器件分类，我们可以在元件库中添加一个分类参数然后通过Library Splitting完成。打开示例文件中*multi-channel_mosfet_and_scr_db.xlsx*, 我们向其中添加新的一列名称叫Component Type，值为器件对应分类。注意这里的分类名称需要和Workplace中定义的名称严格一致。
![define_type_in_xlsx](https://images.gitee.com/uploads/images/2021/1027/111126_41c34a1e_9514082.png "define_type_in_xlsx.png")

保存文件回到Library Migrator，点击Add Library将*multi-channel_mosfet_and_scr_db.DbLib*添加进来。选中刚才添加进来的DB Library点击Library Splitting。在弹出的Splitting对话框中勾上Parameter Grouping，从下拉列表里选中我们刚才添加的Component Type参数，最后执行Update。等待一会儿后我们就可以看见器件都被重新分类了而且迁移路径也被正确定义了。
![group_cmp_by_params](https://images.gitee.com/uploads/images/2021/1027/111207_227665c6_9514082.png "group_cmp_by_params.png")


#### 数据验证
至此我们已经完成了所有的配置工作，元件库已经准备好迁移了。在执行迁移之前不妨点击右上角的Validate对将要上传的数据进行验证。
![output_validate_report](https://images.gitee.com/uploads/images/2021/1027/111258_864f9813_9514082.png "output_validate_report.png")

报告提示我们准备迁移的元件库中存在重复的器件。将重复的器件迁移至Workspace上往往会对后期的内容维护产生巨大的麻烦。在仔细的游览过重复数据之后，我们通过右击冗余器件，选择Exclude From Migration把重复的器件排除在外。

刚才在执行Validate时，Duplicated Component的报告等级是Warning。这一等级的提示并不会阻止迁移指令的执行。但是我们决定不让任何重复的器件进入Workspace。希望在校验过程中如果发现重复内容，那本次迁移不会被执行。为此我们重新打开Properties，在General标签底下找到Migration Checks。其中有一项Component is duplicated by parameter,我们将他的Report Mode从Warning调整为Error。重新执行Validate，这次Migrator没有再给出任何错误报告。
![migration_checks.png](https://images.gitee.com/uploads/images/2021/1027/111356_16d9ed82_9514082.png "migration_checks.png")


#### 迁移
相对于前面漫长的准备工作，最后这个步骤就非常简单了。我们只需要点击Migrate按钮，等待一会儿，就可以在Component面板中看见我们迁移的内容了。也可以打开Explorer在相应的文件夹下面找到这些器件。
![migrate_result_log](https://images.gitee.com/uploads/images/2021/1027/111448_4a19af55_9514082.png "migrate_result_log.png")
![component_pannel](https://images.gitee.com/uploads/images/2021/1027/111503_5ebef889_9514082.png "component_pannel.png")
![result_explorer](https://images.gitee.com/uploads/images/2021/1027/111539_8efe1903_9514082.png "result_explorer.png")